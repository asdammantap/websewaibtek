<?php
  error_reporting(0);

session_start();

if (empty($_SESSION[namauser]) AND empty($_SESSION[pass])){
  echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
  echo "<a href=index.php><b>LOGIN</b></a></center>";
}
else{
?>

<html>
<head>
<title></title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="navbar.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
</head>
<body>
<div id="header">
	 <div class="navbar" style="margin-top:38px;">
                <div class="navbar-inner">
                    <div class="nav-collapse collapse" style="margin-top:5px;">
              <ul class="nav">
                     <li><a href="media.php?module=home">Beranda</a></li>
                
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Input Data<b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="media.php?module=alat">Alat &#187;</a></li>
		<li><a href="media.php?module=mobil">Mobil &#187;</a></li>
		<li><a href="media.php?module=penyewa">Penyewa &#187;</a></li>
		<li><a href="media.php?module=invoice">Tagihan &#187;</a></li>
		<li><a href="media.php?module=downloadspk">SPK &#187;</a></li>
		<li><a href="media.php?module=pengguna">Pengguna &#187;</a></li>
                  </ul>
                </li>
				 <li><a href="media.php?module=laporan">Laporan &#187;</a></li>
        <li><a href="logout.php">&#171; Logout</a></li>
              </ul>
            </div><!--/.nav-collapse -->
                </div>
            </div>

  <div id="content">
		<?php include "content.php"; ?>
  </div>
  
		<div id="footer">
        Jl. Yos Sudarso Link. Kavling No. 30 Lebakgede
		Kecamatan: Pulomerak - Banten
       </div>
</div>

   <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>


</body>
</html>
<?php
}
?>
