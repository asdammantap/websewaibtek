<?php 
  error_reporting(0);
  session_start();	
  include "config/koneksi.php";
	include "config/fungsi_indotgl.php";
	include "config/class_paging.php";
	include "config/fungsi_combobox.php";
	include "config/library.php";
  include "config/fungsi_autolink.php";
  include "config/fungsi_rupiah.php";
  if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
  $user="Pengunjung";
  }
  else
  {
	$user="$_SESSION[namalengkap]";  
  }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>PT. IBTEK (Ibnu Teknik) Cilegon</title>
</script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="index, follow">
<meta http-equiv="imagetoolbar" content="no">
<meta name="language" content="Indonesia">
<meta name="revisit-after" content="7">
<meta name="webcrawlers" content="all">
<meta name="rating" content="general">
<meta name="spiders" content="all">

<link rel="shortcut icon" href="images/mesinlasacdckecil.png" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="" />

<link href="style.css" rel="stylesheet" type="text/css" />
<link href="navbar.css" rel="stylesheet" type="text/css" />
 <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
	<link href="assets/css/vertikalmenu.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

<link href="lightbox/themes/default/jquery.lightbox.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="jquery-1.4.js"></script>
<script type="text/javascript" src="lightbox/jquery.lightbox.min.js"></script>

<script type="text/javascript" src="config/jquery.js"></script>
<script type="text/javascript" src="js/clearbox.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.min14.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/anythingslider.js"></script>
<script type="text/javascript" src="js/jquery.anythingslider.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.2.js"></script>
<script type="text/javascript" src="js/jquery.fancybox-1.3.1.js"></script>
<script type="text/javascript" src="js/contentslider.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/jquery.ad-gallery.js"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<script type="text/javascript" src="js/thumbgallery.js"></script>
<script type="text/javascript" src="js/eurofurence_500-eurofurence_700.font_9bc22cbd.js"></script>
<script type="text/javascript" src="js/cufon.js"></script>

<script type="text/javascript" src="js/newsticker.js"></script>

<!--[if lte IE 7]>
<script type="text/javascript" src="js/jquery.dropdown.js"></script>
<![endif]-->


<script type="text/javascript" src="js/easy.js"></script>
<script type="text/javascript">
	$(document).ready(function(){		
		$.easy.tooltip();	
});
</script>

	<script type="text/javascript">
		$(document).ready(function() {
		    $('.lightbox').lightbox();		    
		});
  </script>

</head>

<body>
<div id="main_container">

	<div id="header"></div>
<!--<span class="borderheader"></span>-->

 <nav id='menu'>
<input type='checkbox'/>
<label>&#8801;<span>Navigation</span></label>

<ul>
<?php
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
echo '
<li><a href="media.php?module=home">Beranda</a></li> 
<li><a href="media.php?module=profilkami">Tentang Kami</a></li> 
<li><a href="media.php?module=semuaalat">Semua Alat</a></li> 
<li><a href="media.php?module=carasewa">Cara Penyewaan</a></li>';

}
else
  {
	echo '
	<li><a href="media.php?module=home">Beranda</a></li> 
<li><a href="media.php?module=profilkami">Tentang Kami</a></li> 
<li><a href="media.php?module=semuaalat">Semua Alat</a></li> 
<li><a href="media.php?module=carasewa">Cara Penyewaan</a></li> 
<li><a href="media.php?module=tagihanid">Invoice</a></li> 
<li><a href="media.php?module=tensipid">Rekap Pemakaian</a></li> ';
}
?>
<div style="padding-top:px;float:right;font-weight:bold;padding-right:20px;text-transform:uppercase;">

<?php
               if (!empty($_SESSION['namauser']) AND !empty($_SESSION['passuser'])){
				?>
                
                  <a href="logout.php" style="color:white;">
                    Logout
                  </a>
               
                <?php
			    }
			    
                if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
				?>  
               
                  <a href="media.php?module=login" style="color:white;">
                    Log in
                  </a>
                
                <?php
				}
				?></div>
</ul>
</nav>

 
    
  <div id="main_content"> 
      <div class="crumb_navigation">
    Anda sedang berada di: <?php include "breadcrumb.php";?>
  </div>     
 

 <!--<div class="left_content"> 
        
	  <?php include "kanan2.php";?>
  </div>  -->
        
   
   <div class="center_content" style="width:750px;">
      <?php include "tengah.php";?>           
   </div>
   
    
            
   </div><!-- end of main content -->
   
   <div class="footer">        
        <div class="left_footer" style="text-align:center;"><center>
        Jl. Yos Sudarso Link. Kavling No. 30 Lebakgede</br>
		Kecamatan: Pulomerak - Banten
        </div>
   </div>                 

</div>
<!-- end of main_container -->
<div style="visibility: hidden; position: absolute;"><div></div></div>

<div style="display: none;">
		<div id="inline1" style="width:400px;">
			<h2>LOGIN ANGGOTA</h2>
    <div class="clear"></div>
    <p>Anda anggota? bila tidak <a href="daftar.html" class="colr2">DAFTAR</a>, di sini.</p>
    <div class="clear"></div>
    <a href="#" class="left"><img src="images/signup.gif" alt="" /></a> &nbsp;<a href="#" class="left"><img src="images/forgot.gif" alt="" /></a>
    <div class="clear marg_bot">&nbsp;</div>
    <ul class="forms">
    	<li class="txt">User Name</li>
        <li class="inputfield"><input type="text" value="" class="bar" /></li>
    </ul>
    <ul class="forms">
    	<li class="txt">Password</li>
        <li class="inputfield"><input type="text" value="" class="bar" /></li>
    </ul>
    <ul class="forms marg_top">
    	<li class="txt"><a href="#" class="simplebtn">Login</a>&nbsp;&nbsp; <a href="#" class="simplebtn">Batal</a></li>
    </ul>
		</div>
	</div>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
	
	 

</body>
</html>
